<?php
/*
	Settings View...
*/
$div_id = unique_id();
$form_id = unique_id();
?>
<form id='<?php echo $form_id;?>' method='post' action='<?php
if (isset($action_url))
	echo $action_url;?>'>
<?php
if (isset($hidden_fields)) {
	foreach ($hidden_fields as $k => $v) {
		echo "<input type='hidden' name='" . form_prep($k) . "' ";
		echo "value='" . form_prep($v) . "' />";
		echo PHP_EOL;
	} // foreach
}


echo $content;

?>
</form>
<script type='text/javascript'><!--
	jQuery(document).ready(function() {
		jQuery("#<?php echo $form_id;?> input").keypress(function(event) {
			if (event.which == 13)
				event.preventDefault();
		} );
		jQuery("#<?php echo $form_id;?>").submit(function() {
			var errors = 0;
			jQuery("#<?php echo $form_id;?> input,#<?php echo $form_id;?> textarea").each(function() {
				var val = String(jQuery(this).val());
				var has_error = 0;
				if (val == "") {
					has_error++;
				} else if (String(jQuery(this).attr("name")).toLowerCase() == "email") {
					if (val.indexOf("@") < 0)
						has_error++;
				}
				
				if (has_error > 0) {
					jQuery(this).css("border-color", "red");
					errors+=has_error;
				} else {
					jQuery(this).css("border-color", "green");
				}
			} );
			if (errors > 0) {
				alert("Please verify form.");
				return false;
			}
			return true;
		} );
	} );
//!--></script>
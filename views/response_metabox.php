<?php
/*
	Settings View...
*/
if (!isset($value))
	$value = array();
?>
<div class='inside'>
	<label>Use Inline Form</label><?php
	$options = array(
		1 => "Handle Processing",
		0 => "Disabled Processing"
	);
	echo form_dropdown(
		"sendinblue[handle_processing]",
		$options,
		safe_arrval("handle_processing", $value, 1)
	);?>
	<p>Use this to enable (or disable) actual processing of this form. You may want to disable it
 if you plan on using inline code you've copied from the sendinblue site. This way, you can make sure
 that all the processing is done on the sendinblue side.</p>
</div>
<div class='inside'>
	<label>Trigger Event</label>
	<input type='text' name='sendinblue[trigger_event]'
	value='<?php echo safe_arrval("trigger_event", $value, "");?>' />
	<p><b>Trigger Event</b> set this to a custom event you want triggered, when the user
 submits this form. This is useful for if you want to track events so that the user
 is added to an automation thing.</p>
</div>
<div class='inside'>
<p>Confirmation Email - enter in a confirmation email that the user will get after
 they successfully enter the form.</p>
<textarea name='sendinblue[response_text]'
id='sendinblue_response_text' cols='80' rows='5'><?php
	echo htmlentities(
		safe_arrval("response_text", $value, "")
	);
?></textarea>
</div>
<div class='inside'>
<label>Success URL</label>
<input type='text' name='sendinblue[success_url]'
value='<?php echo form_prep(safe_arrval("success_url", $value, ""));?>' />
<p style='background-color:white;padding:5px;'><?php
echo htmlentities("Enter the url (if any) upon a successful form submission.");?></p>
</div>
<div class='inside'>
<label>Error URL</label>
<input type='text' name='sendinblue[error_url]'
value='<?php echo form_prep(safe_arrval("error_url", $value, ""));?>' />
<p style='background-color:white;padding:5px;'><?php
echo htmlentities("Enter the url (if any) upon an error form submission.");?></p>
</div>

<div class='inside'>
	<p><?php echo htmlentities("Add/Update Submission To Contact");?></p>
<?php
	$options = array(
		0 => "Do not add",
		1 => "Add/Update Contact"
	);
	echo form_dropdown("sendinblue[add_contact]", $options, intval(safe_arrval('add_contact', $value, 0)));
?>
</div>
<?php
if (isset($sendinblue_lists) && safe_count($sendinblue_lists)) {
?>
<div class='inside'>
	<p><?php
echo htmlentities("Add contact to a list in sendinblue. If it is opted 'Do not add' above," .
 " then this will have no effect." );
	?></p>
<?php
	$current_val = strval(safe_arrval("list", $value, ""));
	$current_val = array_map("intval", explode(",", $current_val));
?>
	<table><?php
		$c = 0;
		foreach ($sendinblue_lists as $row) {
			$nm = 'sendinblue[list][]';
			?><tr>
				<td><!--<input type='hidden' name='<?php
				echo form_prep($nm);?>' value='0'/>!-->
				<input type='checkbox' name='<?php
				echo form_prep($nm);?>' <?php
				if (array_search($row['id'], $current_val) !== FALSE)
					echo "checked='checked' ";
				?> value='<?php echo $row['id'];?>' /></td><td><label><?php echo htmlentities($row['name']);?></label></td>
				</tr><?php
		} // foreach
	?></table>
	<br/>
</div>
<?php
} else {
	?><p style='color:red;'>Could not get lists from sendinblue.</p><?php
}
?>
<div class='inside'>
	<label>Notify Email</label>
	<input name='sendinblue[notify_email]' type='text' value='<?php
echo form_prep(safe_arrval("notify_email", $value, ""));
	?>' />
	<p><?php
echo htmlentities(
	"If a valid email is put here, then this will be notified when this form " .
	"is succesfully submitted, and with the contents entered therein."
);
	?></p>
</div>
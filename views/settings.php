<?php
/*
	Settings View...
*/
$id = unique_id();
?>
<div class='wrap' id='<?php echo $id;?>'>
	<h1>Send In Blue : Settings</h1>
	
	<form id='sendinblue_form' method='post' action=''>
<input type='hidden' name='nonce' value='<?php
	echo form_prep(wp_create_nonce("send_in_blue__settings"));
?>' />
	<p>Use this form to update settings for the sendinblue api.</p>
	
<?php
if (isset($msg) && $msg != "") {
?><p style='border:1px solid grey; padding:10px; color:green; background-color:white;'><?php echo $msg;?></p><?php
} // if
?>
	<table>
		<tr>
			<td><label>API Key</label></td>
			<td>
			<input type='text' name='sendinblue[api_key]'
			value='<?php
			echo form_prep(
				get_option("sendinblue__api_key", "")
			);
			?>' class='form-control'/>
			</td>
		</tr>
		<tr>
			<td><label class='col-md-2'>Cache</label></td>
			<td>
			<select name='sendinblue[use_cache]'>
<?php
	$options = array(
		0 => "Never",
		60 => "1 Minute",
		600 => "10 Minutes",
		1800 => "30 Minutes",
		3600 => "1 Hour"
	);
	$val = intval(get_option("sendinblue__use_cache", 600));
	foreach ($options as $k => $v) {
?><option value='<?php echo $k;?>' <?php
	if ($k == $val) {
		echo "selected='selected' ";
	}
	?> ><?php echo $v;?></option><?php
	echo PHP_EOL;
	} // foreach
?>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
			<p style='background-color:white;border:1px dashed gray; padding:10px'>
This will cache results from the sendinblue api. Caching information such as 
lists, users, etc. This will help reduce the load on the api, and help things
 run a bit faster.</p></td>
		</tr>
		
		<tr>
			<td>From Email</td>
			<td><input type='text' name='sendinblue[from_email]'
			value='<?php echo form_prep(
				get_option("sendinblue__from_email"));?>' />
			</td>
		</tr>
		<tr>
			<td>From Name</td>
			<td><input type='text' name='sendinblue[from_name]'
			value='<?php echo form_prep(
				get_option("sendinblue__from_name"));
				?>' />
			</td>
		</tr>
		<tr>
			<td colspan='2'><p><?php
echo htmlentities("From Email: noreply@gmail.com; From Name: Your Name;");
			?></p></td>
		</tr>
	</table>
	<br/>
	<!--<table>
		<tr>
			<td>Secret Key</td>
			<td><input type='text' name='sendinblue[google_private]'
			value='<?php echo form_prep(
				get_option("sendinblue__google_private"));?>' /></td>
		</tr>
		<tr>
			<td>Public Key</td>
			<td><input type='text' name='sendinblue[google_public]'
			value='<?php echo form_prep(
				get_option("sendinblue__google_public")
			);?>' />
		</tr>
		<tr>
			<td colspan='2'><p>There will be 2 keys google will provide for you <a
href='https://www.google.com/recaptcha/admin' target="_blank">here: <?php echo 
htmlentities("https://www.google.com/recaptcha/admin");?></a>. There will be a <b>public</b> and
 a <b>private</b> or <b>secret</b> key. Please enter these here for usage with the google 
reCaptcha stuff.</p></td>
		</tr>
	</table>!-->
<?php
	//settings_fields("sendinblue-options");
	//do_settings_sections( 'sendinblue-options' );

?>
	
	<?php
	submit_button();
	?>
	</form>
</div>


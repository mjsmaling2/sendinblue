<!--
	Send in blue automation script to include in the header...
!-->
<script type='text/javascript'><!--
 window.sendinblue=window.sendinblue||[];window.sendinblue.methods=["identify","init","group","track","page","trackLink"];window.sendinblue.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);window.sendinblue.push(t);return window.sendinblue}};for(var i=0;i<window.sendinblue.methods.length;i++){var key=window.sendinblue.methods[i];window.sendinblue[key]=window.sendinblue.factory(key)}window.sendinblue.load=function(){if(document.getElementById("sendinblue-js"))return;var e=document.createElement("script");e.type="text/javascript";e.id="sendinblue-js";e.async=true;e.src=("https:"===document.location.protocol?"https://":"http://")+"s.sib.im/automation.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)};window.sendinblue.SNIPPET_VERSION="1.0";window.sendinblue.load();window.sendinblue.client_key="1dri1jj1mti23y1gawnvy";window.sendinblue.page();
//!--></script>
<?php

$current_email = sendinblue_visitor_userdata(TRUE, "email", NULL);
if ($current_email && is_string($current_email)) {
?>
<script type='text/javascript'><!--
	// Identify user...
	sendinblue.identify("<?php echo $current_email;?>");
//!--></script>
<?php
} //

$current_actions = sendinblue_visitor_userdata(TRUE, "track_actions", NULL);
if (safe_count($current_actions) > 0) {
	foreach ($current_actions as $action) {
		if (!is_array($action))
			continue;
?>
<script type='text/javascript'><!--
	sendinblue.<?php echo $action[0];?>(<?php
		echo json_encode($action[1]);
		?>,<?php echo json_encode($action[2]);?>);
//!--></script>
<?php
		echo PHP_EOL;
	} // foreach
	sendinblue_visitor_userdata(TRUE, "track_actions", array());
}

?>

<!-- 
	Events
!-->

<?php

if (defined("SENDINBLUE_PLUGIN_PATH")) {
	add_action('init', 'sendinblue_init');
	add_action("admin_init", "sendinblue_admin_init");
	add_action('admin_menu', 'sendinblue_settings_menu');
	add_action('add_meta_boxes', "sendinblue_add_meta_boxes");
	add_action("save_post", "sendinblue_save_post");
	add_shortcode("sendinblue_form", "sendinblue_shortcode_form");
	add_shortcode("sendinblue_header", "sendinblue_shortcode_header");
}

function sendinblue_init() {
	if (!defined("SENDINBLUE_PLUGIN_PATH"))
		return;
		
	// temp : make sure the tables are created...
//	sendinblue_plugin_activation();
		
	//
	// Register custom taxonomy
	//
	//sendinblue_register_taxonomy("sendinblue", true);
	
	$post_types = array(
		"Send In Blue" => "send-in-blue"
	);
	
	foreach ($post_types as $label => $slug) {
		register_post_type($slug, array(
			'label' => $label,
			'description' => 'Custom post type to deal with sendinblue forms and responses',
			'public' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'has_archive' => false,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array('slug' => $slug, 'with_front' => true),
			'query_var' => true,
			'supports' => array('title','editor', 'author','page-attributes','post-formats'),
			'taxonomies' => array('post'),
			'labels' => array (
				'name' => $label,
				'singular_name' => '',
				'menu_name' => $label,
				'add_new' => "Add $label",
				'add_new_item' => "Add New $label",
				'edit' => 'Edit',
				'edit_item' => "Edit $label",
				'new_item' => "New $label",
				'view' => "View $label",
				'view_item' => "View $label",
				'search_items' => "Search $label",
				'not_found' => 'No ' . $label . ' Found',
				'not_found_in_trash' => 'No ' . $label . ' Found in Trash',
				'parent' => 'Parent ' . $label
			)
		) );
	}
	
	define("SENDINBLUE_VISITOR_ID", sendinblue_visitor_id());
}

function sendinblue_admin_init() {
	if (!defined("SENDINBLUE_PLUGIN_PATH"))
		return;
	//
	// See: https://codex.wordpress.org/Settings_API
	//
	
/*	add_settings_section(
		"sendinblue_settings_section_id",
		"Sendinblue Settings",
		"sendinblue_settings_render"
		
	);*/
	//register_setting(
	//	"sendinblue-settings",
	//	"sendinblue-key",
	//	"sendinblue_setting_callbacks"
	//);
}

function sendinblue_add_meta_boxes() {

	add_meta_box(
		"sendinblue_response",
		"Response",
		"sendinblue_response_metabox_callback",
		"send-in-blue",
		"normal"
	);
}

function sendinblue_settings_menu() {
	if (!defined("SENDINBLUE_PLUGIN_PATH"))
		return;
	add_options_page(
		"Sendinblue Settings",
		"Sendinblue",
		"manage_options",
		"sendinblue_config",
		"sendinblue_settings_render"
	);
}

function sendinblue_settings_render() {
	$view_data = array();
	$msg = "";
	if (count($_POST) > 0) {
		//check_admin_referer("send_in_blue__settings");
		$fields = safe_arrval("sendinblue", $_POST);
		if (safe_count($fields) > 0) {
			foreach ($fields as $k => $v) {
				update_option("sendinblue__" . $k, $v);
			}
			$msg = "Updated : " . safe_count($fields) . " options.";
		}
	}
	$view_data['msg'] = $msg;
	echo render_view(SENDINBLUE_PLUGIN_PATH . "views/settings.php", $view_data);
}

function sendinblue_setting_callbacks($val) {
	$val = trim(strval($val));
	return $val;
}

function sendinblue_response_metabox_callback($post) {
	$post = toArray($post);
	$post_id = intval(safe_arrval(array("ID", "id"), $post, 0));
	$view_data = array(
		"value" => sendinblue_get_post_meta($post_id),
		"sendinblue_lists" => sendinblue_api_get_lists()
	);
	echo render_view(
		SENDINBLUE_PLUGIN_PATH . "views/response_metabox.php",
		$view_data
	);
}

function sendinblue_save_post($post_id, $post = NULL, $update = 0) {
	// If this is just a revision, don't send the email.
	if ( wp_is_post_revision( $post_id ) )
		return;
		
	$post_type = get_post_type($post_id);
	if ($post_type != "send-in-blue")
		return;
	
	$meta = safe_arrval('sendinblue', $_POST);
	if (safe_count($meta) > 0) {
		foreach ($meta as $k => $v) {
			$k = "sendinblue_" . $k;
			switch ($k) {
			case 'sendinblue_list':
				if (is_array($v))
					$v = join(",", $v);
				break;
			default:
				break;
			};
			
			
			update_post_meta($post_id, $k, $v);
		}
	}
}

//
//
//
if (!function_exists("sendinblue_shortcode_header")) {
	//
	// This maybe necessary to add a shortcode so that this can be rendereed
	// in the theme builder thing... AHHHDHDHFDHFDFHDFDHFDH!
	//
	function sendinblue_shortcode_header($args, $content = NULL) {
		$retval = render_view(SENDINBLUE_PLUGIN_PATH . "views/header.php",
			array()
		);
		return $retval;
	}
}
<?php
/*
	Helper functions for Sendinblue plugin...
*/
if (!function_exists('render_view')) {
	function render_view($path, $local_vars = NULL) {
		if ($local_vars !== NULL) {
			if (is_array($local_vars))
				extract($local_vars);
		}
		ob_start();
		if (file_exists($path))
			include($path); // include() vs include_once() allows for multiple views with the same name
		$buffer = ob_get_contents();
		@ob_end_clean();		
		return $buffer;
	}
}

if (!function_exists('form_prep')) {
	//
	// Copied from CIv2 "form_prep" helper function in forms.php
	//
	function form_prep($str = '', $field_name = '') {
		static $prepped_fields = array();

		// if the field name is an array we do this recursively
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = form_prep($val);
			}

			return $str;
		}

		if ($str === '')
		{
			return '';
		}

		// we've already prepped a field with this name
		// @todo need to figure out a way to namespace this so
		// that we know the *exact* field and not just one with
		// the same name
		if (isset($prepped_fields[$field_name]))
		{
			return $str;
		}

		$str = htmlspecialchars($str);

		// In case htmlspecialchars misses these.
		$str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

		if ($field_name != '')
		{
			$prepped_fields[$field_name] = $field_name;
		}

		return $str;
	}
}


if (!function_exists("safe_arrval")) {
	function safe_arrval($keys, $src_array, $default_value = NULL) {
		if (is_object($src_array))
			$src_array = (array) $src_array;
		if (is_array($src_array)) {
			if (!is_array($keys))
				$keys = array($keys);
			foreach ($keys as $k) {
				if (array_key_exists($k, $src_array))
					return $src_array[$k];
			}
		}
		return $default_value;
	}
}

if (!function_exists("safe_count")) {
	function safe_count($src) {
		if (is_object($src))
			$src = (array)$src;
		if (!is_array($src))
			return 0;
		return count($src);
	}
}

if (!function_exists("unique_id")) {
	function unique_id() {
		static $retval = NULL;
		if ($retval === NULL) {
			$retval = array(
				"prefix" => "uid_" . mt_rand(100000, 9999999) . "_",
				"counter" => 0
			);
		}
		$resp = $retval['prefix'] . $retval['counter']++;
		return $resp;
	}
}

if (!function_exists("sendinblue_register_taxonomy")) {
	function sendinblue_register_taxonomy($name, $val = NULL) {
		if (is_array($name)) {
			foreach ($name as $n => $v) {
				sendinblue_register_taxonomy($n, $v);
			}
		} else {
			$label = sendinblue_input_label($name, "-");
			if (taxonomy_exists($label))
				return;
			if (is_bool($val)) {
				$args = array(
					'labels' => array(
						'name' => humanize($name)
					),
					'hierarchical' => $val,
					'update_count_callback' => '_update_post_term_count'
				);
			}
			register_taxonomy(
				$label,
				array('post','blog','partner-content','webinars','gated'),
				$args
			);
		}
	}
}

if (!function_exists("sendinblue_input_label")) {
	/*
		convert $str to a label. 
		This function has been copied from CodeIgniter's url_helper.
	*/
	function sendinblue_input_label($str, $separator = "_", $lowercase = TRUE)
	{
		$q_separator = preg_quote($separator);

		$trans = array(
			'&.+?;'                 => '',
			'[^a-z0-9 _-]'          => '',
			'\s+'                   => $separator,
			'('.$q_separator.')+'   => $separator
		);

		$str = strip_tags($str);

		foreach ($trans as $key => $val) {
			$str = preg_replace("#".$key."#i", $val, $str);
		}

		if ($lowercase === TRUE)
			$str = strtolower($str);

		return trim($str, $separator);
	}
}
/*
if (!function_exists("sendinblue_register_post_type")) {
	function sendinblue_register_post_type($label, $slug = NULL) {
		if (is_array($name)) {
			foreach ($label as $k => $v)
				sendinblue_register_post_type($k, $v);
		} else {
			register_post_type($slug, array(
				'label' => $label,
				'description' => '',
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'has_archive' => true,
				'capability_type' => 'post',
				'map_meta_cap' => true,
				'hierarchical' => false,
				'rewrite' => array('slug' => $slug, 'with_front' => true),
				'query_var' => true,
				'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
				'taxonomies' => array('category','post_tag','company'),
				'labels' => array (
					'name' => $label,
					'singular_name' => '',
					'menu_name' => $label,
					'add_new' => "Add $label",
					'add_new_item' => "Add New $label",
					'edit' => 'Edit',
					'edit_item' => "Edit $label",
					'new_item' => "New $label",
					'view' => "View $label",
					'view_item' => "View $label",
					'search_items' => "Search $label",
					'not_found' => 'No ' . $label . ' Found',
					'not_found_in_trash' => 'No ' . $label . ' Found in Trash',
					'parent' => 'Parent ' . $label
				)
			) );
		}
	} // foreach
}
*/

if (!function_exists("humanize")) {
	function humanize($str)
	{
		return ucwords(preg_replace('/[_]+/', ' ', strtolower(trim($str))));
	}
}

if (!function_exists("sendinblue_obj")) {
	function sendinblue_obj($key = NULL) {
		static $retval = NULL;
		if (!class_exists("Mailin"))
			include_once(SENDINBLUE_PLUGIN_PATH . "libs/V2.0/Mailin.php");
		if ($retval === NULL || $key !== NULL) {
			if ($key === NULL) {
				$key = strval(get_option("sendinblue__api_key", ""));
				if ($key === "")
					return FALSE;
			}
			//echo "KEY: $key<br/>"; exit(0);
			$retval = new Mailin(
				"https://api.sendinblue.com/v2.0",
				$key,
				10000
			);
		}
		return $retval;
	}
}

if (!function_exists("sendinblue_api_cache_user")) {
	function sendinblue_api_cache_user($data) {
		$email = safe_arrval(array("EMAIL", "email"), $data, "");
		$key = "users-" . $email;
		return sendinblue_cache($key, $data);
	}
}

if (!function_exists('sendinblue_api_get_user')) {
	function sendinblue_api_get_user($email, $refresh = false) {
		$key = "users-" . $email;
		
		$retval = sendinblue_cache($key);
		
		if (safe_count($retval) <= 0 || $refresh === TRUE) {
			$cls = sendinblue_obj();
			if (!is_object($cls))
				return NULL;
			$data = array("email" => $email);
			
			$resp = $cls->get_user($data);
			sendinblue_log("sendinblue_api_get_lists:");
			sendinblue_log($data);
			sendinblue_log($resp);
			
			$retval = safe_arrval("data", $resp, NULL);
			$retval = safe_arrval("attributes", $retval, NULL);
			if (safe_count($retval) > 0)
				sendinblue_api_get_user($key, $retval);
		}
		if (safe_count($retval) <= 0)
			return NULL;
		return $retval;
	}
}

if (!function_exists("sendinblue_get_lists")) {
	function sendinblue_api_get_lists() {
		$key = "lists";
		$retval = sendinblue_cache($key);
		if (safe_count($retval) <= 0) {
			// retrieve signal from api...
			$cls = sendinblue_obj();
			if (!is_object($cls)) {
				return NULL;
			}
			$config = array(
				"page" => 0,
				"page_limit" => 50	// 50 is the maximum
			);

			$retval = $cls->get_lists($config);
			
			sendinblue_log("sendinblue_api_get_lists:");
			sendinblue_log(($config));
			sendinblue_log(($retval));
			
			sendinblue_cache($key, $retval);
		}
		
		$resp =  toArray(safe_arrval('data', $retval));
		$retval = array();
		while (safe_count($resp) > 0) {
			$row = array_shift($resp);
			$retval[$row['id']] = $row;
		}
		return $retval;
	}
}

if (!function_exists("sendinblue_cache")) {
	//
	// Sets/returns cechce...
	// $key - the key in the cahce..
	// $set - optional - set the cache to this.
	// $default_val - 
	//
	function sendinblue_cache($key, $set = NULL, $default_val = NULL) {
		global $wpdb;
		$use_cache = intval(get_option("sendinblue__use_cache", 0));
		if ($use_cache <= 0)
			return $default_val;
		$t1 = "sendinblue_cache";
		if ($set !== NULL) {
			$set = json_encode($set);
			$sql = "SELECT id FROM $t1 WHERE keyname=%s";
			$sql = $wpdb->prepare($sql, $key);
			$id = intval($wpdb->get_var($sql, 0, 0));
			$dt = date("Y-m-d H:i:s", time());
/* "keyname VARCHAR(100)," . 
				"value TEXT," . 
				"updated DATETIME," . */
			if ($id <= 0) {
				$sql = "INSERT INTO $t1 (keyname, value, updated) VALUES (" .
					"%s, %s, %s)";
				$sql = $wpdb->prepare($sql, $key, $set, $dt);
				
			} else {
				$sql = "UPDATE $t1 SET value=%s, updated=%s WHERE id=%d";
				$sql = $wpdb->prepare($sql, $set, $dt, $id);
			}
			$wpdb->query($sql);
		}
		
		//
		// Return cache...
		//
		$dt = date("Y-m-d H:i:s", time() - $use_cache); 
		$sql = "SELECT value FROM $t1 WHERE keyname=%s AND updated > %s";
		$sql = $wpdb->prepare($sql, $key, $dt);
		$row = $wpdb->get_row($sql, ARRAY_A);
		if (!is_array($row))
			return $default_val;
		$retval = $row['value'];
		return json_decode($retval, true);
	}
}

if (!function_exists("toArray")) {
	function toArray($src, $traverse = 0) {
		if (is_object($src))
			$src = (array) $src;
		if (is_array($src) && $traverse > 0) {
			$traverse--;
			foreach ($src as &$val)
				$val = toArray($val, $traverse);
		}
		return $src;
	}
}


if (!function_exists('form_dropdown')) {
	function form_dropdown($name, $options, $value, $extra = "") {
		$retval = "<select name='" . form_prep($name) . "' $extra >";
		foreach ($options as $k => $v) {
			$retval .= "<option value='" . form_prep($k) . "' ";
			if ($k == $value) {
				$retval .= "selected='selected' ";
			}
			$retval .= ">" . htmlentities($v) . "</option>" . PHP_EOL;
		}
		$retval .= "</select>" . PHP_EOL;
		return $retval;
	}
}

if (!function_exists("sendinblue_get_post_meta")) {
	function sendinblue_get_post_meta($post_id) {
	
		$post = sendinblue_get_post($post_id, 'ID');
		if (!is_array($post))
			return array();
	
		global $wpdb;
		
		$retval = array();
		$post_id = intval($post['ID']);
		
		$prefix = "sendinblue_";
		
		$t1 = $wpdb->prefix . "postmeta";
		$sql = "SELECT meta_key, meta_value FROM $t1 WHERE post_id=%d " .
			"AND meta_key LIKE %s";
		$sql = $wpdb->prepare($sql, $post_id, $prefix . "%");
		
		$rows = $wpdb->get_results($sql, ARRAY_A);
		
	//	echo $sql . " " . json_encode($rows); exit(0);
		
		while (safe_count($rows) > 0) {
			$row = array_shift($rows);
			$retval[ substr($row['meta_key'], strlen($prefix)) ] = $row['meta_value'];
		}

		return $retval;
	}
}

if (!function_exists("valid_email")) {
	function valid_email($email)
	{
		return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
	}
}


if (!function_exists("sendinblue_add_contact")) {
	//
	// $user - should have email, and name.
	// will return ID of user added, or FALSE on error.
	//
	function sendinblue_add_contact($user, &$error = "") {
		$email = trim(safe_arrval(array("EMAIL", 'email'), $user, ""));
		if ($email == "" || !valid_email($email)) {
			$error = "Invalid email.";
			return FALSE;
		}

		$attributes = sendinblue_api_get_user($email);
		if (!is_array($attributes) || safe_count($attributes) <= 0)
			$attributes = array();

		$accept = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		foreach ($user as $k => $v) {
			$missed = 0;
			for ($x = 0; $x < strlen($k); $x++) {
				$ch = substr($k, $x, 1);
				if (strpos($accept, $ch) === FALSE) {
					$missed++;
					break;
				}
			}
			if ($missed <= 0)
				$attributes[$k] = $v;
		}
		$attributes['NAME'] = safe_arrval("NAME", $attributes, "");
		if ($attributes['NAME'] == "") {
			$name = array();
			foreach (explode("|", "FIRSTNAME|SURNAME") as $k) {
				$val = safe_arrval($k, $attributes, "");
				if ($val != "")
					$name[] = $val;
			}
			$attributes['NAME'] = join(" ", $name);
		}
		$attributes['LAST_CHANGED'] = date("Y-m-d H:i:s");
		
		$config = array();
		$config['email'] = $email;
		$config['attributes'] = $attributes;
		
		$cls = sendinblue_obj();
		if (!is_object($cls)) {
			$error = "Could not instantiate mailin class.";
			return FALSE;
		}
		
		
		$resp = $cls->create_update_user($config);
		sendinblue_log("ADD CONTACT:");
		sendinblue_log(($config));
		sendinblue_log(($resp));
		
		$data = safe_arrval('data', $resp, "");
		$id = intval(safe_arrval("id", $data, 0));
		
		if ($id <= 0) {
			$error = safe_arrval("message", $resp, "Error");
			return FALSE;
		}
		
		//
		// Cache user attritbues...
		//
		sendinblue_api_cache_user($attributes);
		
		return $id;
	}
}

if (!function_exists("sendinblue_add_user_to_list")) {
	function sendinblue_add_user_to_list($email, $list_id, &$error = "") {
		sendinblue_log("sendinblue_add_user_to_list");
		
		if (is_array($email))
			$email = safe_arrval(array("EMAIL", 'email'), $email, "");
		if ($email == "" || !valid_email($email)) {
			$error = "Invalid email.";
			return FALSE;
		}
		
		//
		// Make sure this list is valid...
		//
		$current_lists = sendinblue_api_get_lists();
//		if (!isset($current_lists[$list_id])) {
//			$error = "Invalid list id.";
//			return FALSE;
//		}
		
		$config = array(
			//"id" => $list_id,
			"users" => array($email)
		);
		$list_id = explode(",", $list_id);
		
		$cls = sendinblue_obj();
		if (!is_object($cls)) {
			$error = "Could not instantiate mailin class.";
			return FALSE;
		}
		
		foreach ($list_id as $id) {
		
			if (isset($current_lists[strval($id)])) {
				
				$config['id'] = $id;
				$resp = $cls->add_users_list($config);
			
				sendinblue_log("Add Users List");
				sendinblue_log(($config));
				sendinblue_log(($resp));
			
			//	echo "RESP:"; print_r($resp);
			} else {
			//	echo "Could not find $id in " . json_encode($current_lists);
			}
		}
//		exit(0);
		
		return TRUE;
	}
}

if (!function_exists("sendinblue_send_email")) {
	function sendinblue_send_email($to_email, $subject, $message, &$error = "") {
		$config = array();
		$config['to'] = array(humanize($to_email) => $to_email);
		$config['subject'] = $subject;
		$config['from'] = array(
			strval(get_option("sendinblue__from_email")),
			strval(get_option("sendinblue__from_name"))
		);
		$config['html'] = $message;
		
		
		$cls = sendinblue_obj();
		if (!is_object($cls)) {
			$error = "Could not initiate object.";
			return FALSE;
		}
		
		// temproary...
		$resp = $cls->send_email($config);
		sendinblue_log("Send Email");
		sendinblue_log($config);
		sendinblue_log($resp);
		//echo "SEND_EMAIL: "; print_r($config); echo json_encode($resp); echo "<Br/>";
		
		return $resp;
	}
}

if (!function_exists("sendinblue_captcha")) {
	//
	// Provides code for captaha solvants...
	//
	function sendinblue_captcha() {
		$a = mt_rand(1, 12);
		$b = mt_rand(1, 12);
		if ($b > $a) {
			$c = $a;
			$a = $b;
			$b = $c;
		}
		
		$c = mt_rand(1, 3);
		switch($c) {
		case 3: // minus
			$c = $a - $b;
			$operand = "minus";
			break;
		case 2: // multiply;
			$c = $b * $a;
			$operand = "multiply";
			break;
		case 1: // plus
		default:
			$c = $a + $b;
			$operand = "plus";
			break;
		}
		
		$now = time();
		$set = array(
			'text' => $a . " " . $operand . " " . $b,
			'answer' => $c,
			"sine_answer" => captcha_answer($c)
		);
		return $set;
	}
}

if (!function_exists("captcha_answer")) {
	//
	// Usually, you would store the actual answer the captcha in $_SESSION or something,
	// but wordpress has it's own session stuff... I didn't want to bother with it right now,
	// so, this is made to "hide" the answer as a sine of the actual answer.
	//
	function captcha_answer($result) {
		if (!is_numeric($result))
			return -1;
		$result = intval(substr($result, 0, 5));
		$result = floor(sin(deg2rad($result)) * 1000);
		return $result;
	}
}

if (!function_exists("sendinblue_add_query")) {
	function sendinblue_add_query($src_url, $query) {
		if (is_array($query))
			$query = http_build_query($query);
		if (strpos($src_url, "?") === FALSE)
			$src_url .= "?";
		else
			$src_url .= "&";
		$src_url .= $query;
		return $src_url;
	}
}

if (!function_exists('sendinblue_render_form')) {
	function sendinblue_render_form($id, $by = 'id') {
		$attr = array(
			$by => $id
		);
		return sendinblue_shortcode_form($attr, "");
	}
}

if (!function_exists("sendinblue_log")) {
	function sendinblue_log($line, $eol = TRUE) {
		if (!defined("SENDINBLUE_PLUGIN_LOG"))
			return;
		static $fp = NULL, $start_t, $c;
		if ($fp === NULL) {
			$start_t = microtime(true);
			$c = 0;
			$fn = SENDINBLUE_PLUGIN_PATH . "log.txt";
			
			if (is_file($fn) && filesize($fn) > ((1024 * 1024) * 5))
				$fp = fopen($fn, "w");
			else
				$fp = fopen($fn, "a");
		}
		if ($fp) {
			$t = "[" . $c++ . "\t" . sprintf("%.4f", (microtime(true) - $start_t)) . "]\t";
			fwrite($fp, $t);
			fwrite($fp, json_encode($line) . PHP_EOL);
		} else {
			echo "Could not open file... $fn"; exit(0);
		}
	}
}

if (!function_exists("sendinblue_get_post")) {
	//
	// This will get the most valid post of thingy...
	//
	// Perahps the particular post_id is in "inherit" mode, so we want to get
	// the latest post of this... So that its upto date thing.
	//
	function sendinblue_get_post($val, $by = 'auto', $post_type = "") {
		$retval = FALSE;
		if ($by == 'auto') {
			if (is_int($val))
				$by = 'ID';
			else
				$by = 'post_title';
		}
		global $wpdb;
		$attempts = 0;
		while ($attempts++ < 2) {
			$sql = "SELECT * FROM " . $wpdb->posts . " WHERE ";
			if ($attempts == 1) {
				if ($by == 'ID') {
					$sql .= "ID=" . intval($val);
				} else {
					$sql .= $by="%s";
					$sql = $wpdb->prepare($sql, $val);
				}
			} else if ($attempts == 2) {
				$sql .= "post_name=%s AND post_status IN (%s, %s)";
				$sql = $wpdb->prepare($sql, $retval['post_name'], "draft", "publish");
			}
			
			if ($post_type != "") {
				$sql .= " AND post_type=%s";
				$sql = $wpdb->prepare($sql, $post_type);
			}
			
			$sql .= " ORDER BY post_modified DESC";
			
			$retval = $wpdb->get_row($sql, ARRAY_A);
			if (safe_count($retval) <= 0) {
				$retval = FALSE;
				break;
			}
			
			if (safe_arrval("post_status", $retval, "") == "publish")
				break;
		}
		return $retval;
	}
}

if (!function_exists("sendinblue_shortcode_form")) {
	function sendinblue_shortcode_form($attr, $content = NULL) {
		global $wpdb;
		$t1 = $wpdb->posts;
		$post_type = "send-in-blue";
		
		$attr = shortcode_atts(array(
			"id" => 0,
			"title" => "",
			"name" => ""
		), $attr);
		
		$content = strval($content);
		
		$translate = array(
			"id" => "ID",
			"title" => "post_title",
			"name" => "post_name"
		);
		
		//
		// Try and determine which shortcode this thing is...
		//
		$id = intval($attr['id']);
		$post = NULL;
		foreach ($attr as $k => $v) {
			$sql = "";
			$k = safe_arrval($k, $translate, "");
			if ($k == "")
				continue;
			if (strval($v) == "")
				continue;
			$post = sendinblue_get_post($v, $k, $post_type);
		}
		if (safe_count($post) <= 0) {
			$content .= "<p style='color:red; display:inline;'>Could not determine post.</p>";
			return $content;
		}
		
		$hidden_values = array();
		$hidden_values['nonce_a'] = "_info_" . unique_id();
		$hidden_values['nonce_b'] = wp_create_nonce($hidden_values['nonce_a']);
		$hidden_values[ $hidden_values['nonce_a'] ] = $post['ID'];

		
		$meta = sendinblue_get_post_meta($post['ID']);
		//$sendinblue_config = sendinblue_get_post_meta($sendinblue_post_id);
		$handle = intval(safe_arrval("handle_processing", $meta, 1));
		$content = $content . $post['post_content'];

		// see if we should handle this...
		if ($handle <= 0)
			return $content;

		$look_for = "[captcha]";
		$has_captcha = stripos($content, $look_for);
		if ($has_captcha !== FALSE) {
			$details = sendinblue_captcha();
			$captcha = "<label>Solve this math problem: <b>" . $details['text'] . "</b></label>";
			$captcha .= "<input type='text' name='captcha' value='' placeholder='Enter numeric value'/>";
			$left = substr($content, 0, $has_captcha);
			$right = substr($content, $has_captcha + strlen($look_for));
			$content = $left . $captcha . $right;
			$hidden_values[ "nonce_c" ] = $details['sine_answer'];
		}
		
		
		$view_data = array(
			"config" => sendinblue_get_post_meta($post['ID']),
			"content" => $content,
			"action_url" => SENDINBLUE_PLUGIN_URL . "/process.php",
			"hidden_fields" => $hidden_values
		);
		
		$content = render_view(
			SENDINBLUE_PLUGIN_PATH . "views/shortcode_render.php",
			$view_data
		);
		
		return $content;
	}
}

if (!function_exists("sendinblue_process")) {
	function sendinblue_process($query) {
		if (safe_count($query) <= 0)  {
			wp_redirect( home_url() . "?error=invalid_query");
			return -1;
		}

		//
		// Confirm NONCE...
		//
		$nonce_key = safe_arrval("nonce_a", $query, "");
		$nonce_value = safe_arrval("nonce_b", $query, "");
		$sendinblue_post_id = intval(safe_arrval($nonce_key, $query, 0));
		if ($sendinblue_post_id <= 0) {
			wp_redirect( home_url() . "?error=invalid_post_id");
			return -2;
		}

		$sendinblue_post = get_post($sendinblue_post_id, ARRAY_A);
		if (!is_array($sendinblue_post)) {
			wp_redirect( home_url() . "?error=invalid_post");
			return -3;
		}
		
		$sendinblue_config = sendinblue_get_post_meta($sendinblue_post_id);
		$handle = intval(safe_arrval("handle_processing", $sendinblue_config, 1));
		if ($handle <= 0) {
			wp_redirect( home_url() . "?error=opt_no_handle");
			return -4;
		}

		$err_url = safe_arrval("error_url", $sendinblue_config, "");
		$slash = "/";
		if ($err_url == "")
			$err_url = home_url();
		else if (strncmp($err_url, $slash, strlen($slash)) == 0)
			$err_url = site_url($err_url);
		$ok_url = safe_arrval("success_url", $sendinblue_config, "");
		if ($ok_url == "")
			$ok_url = home_url();
		else if (strncmp($ok_url, $slash, strlen($slash)) == 0) {
			$ok_url = site_url($ok_url);
		}
		
		$message = $sendinblue_post['post_content'];
		if (stripos($message, "[captcha]") !== FALSE) {
			$nonce_c = strval(safe_arrval("nonce_c", $query, ""));
			$captcha_check = strval(safe_arrval("captcha", $query, ""));
			if (captcha_answer($captcha_check) != $nonce_c) {
				wp_redirect($err_url . "?error=invalid_captcha_answer");
				return -3;
			}
		}

		$sendinblue_post = get_post($sendinblue_post_id, ARRAY_A);
		
		$errors = array();
		foreach ($query as $k => $v) {
			$v = trim($v);
			if ($k == "EMAIL" || $k == "email") {
				if (!valid_email($v)) {
					$errors[] = "Invalid $k ($v).";
				}
			}
		}
		
	//	print_r($sendinblue_config);
		$add_contact = intval(safe_arrval("add_contact", $sendinblue_config, 0));
		if ($add_contact > 0) {
			$resp = sendinblue_add_contact($query, $err);
			if ($resp  === FALSE) {
				$errors[] = $err;
			} else {
				$add_to_list = trim(safe_arrval("list", $sendinblue_config, ""));
				if (strlen($add_to_list) > 0) {
					$resp = sendinblue_add_user_to_list($query, $add_to_list, $err);
					if ($resp === FALSE) {
						$errors[] = $err;
					}
					
				} else {
		//			echo "opted no add $add_to_list"; exit(0);
				}
			}
		}

		if (count($errors) <= 0) {
		
			//
			// Trigger Events...
			//
			$email = safe_arrval(array("EMAIL", "email"), $query, "");
			$trigger_event = trim(safe_arrval("trigger_event", $sendinblue_config, ""));
			if ($trigger_event != "" && $email != "") {
				sendinblue_api_trigger_events($email, $trigger_event);
			}
			
			if (valid_email($email)) {
				sendinblue_visitor_userdata(TRUE, "email", $email);
			}

			// see if a notify email should be triggered...
			$notify_email = safe_arrval("notify_email", $sendinblue_config, "");
			if (valid_email($notify_email)) {
				$message = "Notification for form: " . $sendinblue_post['post_title'] . "<br/>";
				$message .= "<table><tbody>";
				$skip = array(
					$nonce_key, "nonce_a", "nonce_b"
				);
				foreach ($query as $k => $v) {
					if (array_search($k, $skip) !== FALSE)
						continue;
					$message .= "<tr><td><b>" . htmlentities($k) . "</b></td><td>";
					$message .= htmlentities($v) . "</td></tr>";
				}
				$message .= "</tbody></table>";
				$resp = sendinblue_send_email(
					$notify_email,
					$sendinblue_post['post_title'] . " Submission",
					$message,
					$err
				);
				if ($resp === FALSE)
					$errros[] = $resp;
			}
		}
		
		if (safe_count($errors) > 0) {
			$errors = join(" ", $errors);
			$url = sendinblue_add_query($err_url, array("error" => $errors));
			
			wp_redirect($url );
			return -4;
		}
		
		$response_text = trim(safe_arrval("response_text", $sendinblue_config, ""));
		$email = safe_arrval("email", $query, "");
			
		if ($response_text != "" && valid_email($email)) {
			$message = htmlentities($response_text);
			sendinblue_send_email($email, $sendinblue_post['post_title'], $message, $err);
		}
		
		//echo "Did this work?"; exit(0);
		
		$url = sendinblue_add_query($ok_url, array("msg" => "OK"));
		wp_redirect($url);
		return 1;
	}

}

if (!function_exists('sendinblue_api_trigger_events')) {
	
	function sendinblue_api_trigger_events($email, $events) {
		if (!valid_email($email)) {
			sendinblue_log("sendinblue_trigger_events invalid email $email");
			return -1;
		}
		if (!function_exists("curl_init")) {
			sendinblue_log("sendinblue_trigger_events require curl.");
			return -1;
		}
		if (!is_array($events))
			$events = explode(",", $events);
		$user = sendinblue_api_get_user($email);

		if (safe_count($user) <= 0) {
			sendinblue_log("sendinblue_trigger_events could not get user info.");
			sendinblue_log($user);
			return -2;
		}
		$use_frontend = 0;
		$use_frontend++;
		include_once(SENDINBLUE_PLUGIN_PATH . "libs/sendinblue.php");
		
		$api_key = get_option("sendinblue__api_key", "");
//		$api_key = "1dri1jj1mti23y1gawnvy";
		
		$cls = new Sendinblue($api_key, sendinblue_visitor_id());
		
		foreach ($events as $event) {
			$event = trim($event);
			if ($event == "")
				continue;
			sendinblue_log("sendinblue_trigger_events: track: $event");
		
			if ($use_frontend > 0) {
				$data = NULL;
				sendinblue_log("sendinblue_trigger_events: Use frontend only.");
			} else {
				$data = array();
				$data['email_id'] = safe_arrval(array("EMAIL", "email"), $user, "");
				$cls->identify($data);
				$data['name'] = $event;
				$data = $cls->track($data);
			}
			if ($data === NULL || $data === "") {
			//	echo "Could not track... url is: " . $cls->_url; exit(0);
				if ($use_frontend <= 0) {
					sendinblue_log("sendinblue_trigger_events: could not track via php.");
					sendinblue_log("Errno: " . $cls->_errno . "\nErrmsg: " . $cls->_errmsg);
				}
		
				$actions = sendinblue_visitor_userdata(TRUE, "track_actions");
				if (!is_array($actions))
					$actions = array();
				$actions[] = array("track", $event, array());
				sendinblue_visitor_userdata(TRUE, "track_actions", $actions);
		
			}
//			echo "DATA:"; print_r($data); exit(0);
			curl_close($ch);
		}
	}
}

if (!function_exists("sendinblue_visitor_userdata")) {
	function sendinblue_visitor_userdata($visitor_id = TRUE, $key = NULL, $set = NULL) {
		include_once(SENDINBLUE_PLUGIN_PATH . "db.php");
		$db = new Sendinblue_db();
		
		if ($visitor_id === TRUE)
			$visitor_id = sendinblue_visitor_id();
		
		$row = $db->get_visitor_row($visitor_id);
		$userdata = safe_arrval("user_data", $row);
		if ($key !== NULL) {
			if ($set !== NULL) {
				$userdata[$key] = $set;
				$new_row = array(
					"id" => $row['id'],
					"user_data" => $userdata
				);
				$db->update_visitor($row['id'], $new_row);
			}
			$userdata = safe_arrval($key, $userdata, NULL);
		}
		return $userdata;
	}
}

if (!function_exists("sendinblue_visitor_id")) {
	function sendinblue_visitor_id() {
		static $retval = NULL;
		if ($retval === NULL) {
			include_once(SENDINBLUE_PLUGIN_PATH . "db.php");
			$db = new Sendinblue_db();
			
			$visitor_id = trim(safe_arrval("sendinblue_visitor_id", $_COOKIE, ""));

			$user_agent = safe_arrval("HTTP_USER_AGENT", $_SERVER, "- Unknown User Agent -");
			$ip_address = safe_arrval("REMOTE_ADDR", $_SERVER, "- Unknown Remote Addr -");
			
			$row = $db->get_visitor_row(NULL, $visitor_id, $user_agent, $ip_address);
			
			$retval = intval(safe_arrval("id", $row, 0));
			
			if ($retval <= 0) {
				$db->delete_expired_visitors();
				
				$visitor_id = random_string("unique");
				while (strlen($visitor_id) < 64)
					$visitor_id .= "-" . strtoupper(random_string("alpha", 4));
				
				$retval = $db->create_visitor($visitor_id, $user_agent, $ip_address);
				
				
			} else {
				$db->update_visitor($retval);
			}
			
			setcookie("sendinblue_visitor_id", $visitor_id, time() + 3600, "/");

		}
		return $retval;
	}
}



if ( ! function_exists('random_string'))
{
	/**
	 * Create a Random String
	 *
	 * Useful for generating passwords or hashes.
	 *
	 * @param	string	type of random string.  basic, alpha, alnum, numeric, nozero, unique, md5, encrypt and sha1
	 * @param	int	number of characters
	 * @return	string
	 */
	function random_string($type = 'alnum', $len = 8)
	{
		switch ($type)
		{
			case 'basic':
				return mt_rand();
			case 'alnum':
			case 'numeric':
			case 'nozero':
			case 'alpha':
				switch ($type)
				{
					case 'alpha':
						$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'alnum':
						$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'numeric':
						$pool = '0123456789';
						break;
					case 'nozero':
						$pool = '123456789';
						break;
				}
				return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
			case 'unique': // todo: remove in 3.1+
			case 'md5':
				return md5(uniqid(mt_rand()));
			case 'encrypt': // todo: remove in 3.1+
			case 'sha1':
				return sha1(uniqid(mt_rand(), TRUE));
		}
	}
}



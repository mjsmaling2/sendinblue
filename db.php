<?php

if (!class_exists("Sendinblue_db")) {

class Sendinblue_db {
	var $_wpdb;
	
	var $_visitor_table;
	var $_expires_dt;
	function __construct() {
		global $wpdb;
		$this->_wpdb = &$wpdb;
		
		$this->_visitor_table = "sendinblue_visitors";
		$this->_expires_dt = $this->dt(time() + 3600);
	}
	
	public function get_visitor_row($id = NULL, $identity = NULL, $user_agent = NULL, $ip_address = NULL) {
		$t1 = $this->_visitor_table;
		
		$sql = "SELECT $t1.* FROM $t1 ";
		$where = array();
		if ($id !== NULL)
			$where["$t1.id"] = $id;
		if ($identity !== NULL)
			$where["$t1.identity"] = $identity;
		if ($user_agent !== NULL)
			$where["$t1.user_agent"] = $user_agent;
		if ($ip_address !== NULL)
			$where["$t1.ip_address"] = $ip_address;
		$where = $this->build_where($where);
		if ($where != "")
			$where .= " AND ";
		else
			$where .= " WHERE ";
		$where .= " expires > " . $this->escape($this->dt());
		$sql .= $where;
		$row =  $this->_wpdb->get_row($sql, ARRAY_A);
		$row['user_data'] = @json_decode($row['user_data'], TRUE);
		
	//	echo $this->_wpdb->last_query; exit(0);
		return $row;
	}

	public function update_visitor($id, $set = array()) {
		$t1 = $this->_visitor_table;
		$sql = "UPDATE $t1 SET $t1.expires=" . $this->escape($this->dt(time() + 3600));
		foreach ($set as $k => $v) {
			if ($k == "user_data")
				$v = json_encode($v);
			$sql .= ", $t1.$k = " . $this->escape($v);
		}
		$sql .= " WHERE id=$id";
		$retval = $this->_wpdb->query($sql);
		return $retval;
	}
	
	public function delete_expired_visitors() {
		$t1 = $this->_visitor_table;
		$sql = "DELETE FROM $t1 WHERE $t1.expires < " . $this->escape($this->dt());
		return $this->_wpdb->query($sql);
	}
	
	public function create_visitor($identity, $user_agent, $ip_address, $user_data = NULL) {
		$t1 = $this->_visitor_table;
		$user_data = json_encode($user_data);
		$values = array(
			"user_agent" => $user_agent,
			"identity" => $identity,
			"ip_address" => $ip_address,
			"user_data" => $user_data,
			"expires" => $this->dt(time() + 3600)
		);
		$resp = $this->_wpdb->insert($t1, $values, array("%s", "%s", "%s", "%s", "%s"));
		return $this->_wpdb->insert_id;
	}
	
	
	public function build_where($where) {
		$retval = array();
		foreach ($where as $k => $v) {
			$item = "$k ";
			if (is_array($v)) {
				$item .= " IN (";
				$v = array_map(array($this, "escape"), $v);
				$item .= join(",", $v);
				$item .= ")";
			} else {
				$item .= " = " . $this->escape($v);
			}
				
			$retval[] = $item;
		}
		if (count($retval) <= 0)
			return "";
		return "WHERE " . join(" AND ", $retval);
	}
	
	public function escape($val) {
		if (is_numeric($val))
			return $val;
		return $this->_wpdb->prepare("%s", $val);
	}
	
	public function dt($t = NULL) {
		if ($t === NULL)
			$t = time();
		return date("Y-m-d H:i:s", $t);
	}
	
	
};

} // if
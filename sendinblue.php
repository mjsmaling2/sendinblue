<?php
/*
Plugin Name: Sendinblue WP Plugin 
Plugin URI: #
Description: A plugin to help with plugging in Sendinblue functionality. Developed for IWOL site.
Version: 0.1
Author: Annonomous
*/

//error_reporting(E_ALL | E_PARSE | E_WARNING | E_STRICT);ini_set("display_errors", 1);

define('SENDINBLUE_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('SENDINBLUE_PLUGIN_URL', plugin_dir_url(__FILE__));

//define('SENDINBLUE_PLUGIN_LOG', 1);

// contact_form_submit
include_once(SENDINBLUE_PLUGIN_PATH . "functions.php");
include_once(SENDINBLUE_PLUGIN_PATH . "init.php");

function sendinblue_plugin_activation() {
	global $wpdb;
	
	//
	// Create some tables if not exists...
	//
	$sqls = array(
		"sendinblue_cache" => "(id int not null AUTO_INCREMENT," .
				"keyname VARCHAR(100)," . 
				"value TEXT," . 
				"updated DATETIME," . 
				"PRIMARY KEY(id) )",
		"sendinblue_visitors" => "(id int not null AUTO_INCREMENT, " .
				"identity VARCHAR(100), " .
				"user_agent VARCHAR(255), " . 
				"ip_address VARCHAR(100), " . 
				"user_data TEXT," . 
				"expires DATETIME," . 
				"PRIMARY KEY(id) )"
	);
	
	foreach ($sqls as $table => $sql) {
	// $sql = "SHOW TABLES LIKE '" . $table . "'";
		$row = $wpdb->get_row("SHOW TABLES LIKE '" . $table . "'", ARRAY_A);
		if (!is_array($row)) {
			$sql = "CREATE TABLE $table $sql";
			$wpdb->query($sql);
		}
	}
	
}
function sendinblue_plugin_deactivation() {
	global $wpdb;
	//
	// Drop some tables...
	//
	$tables = array("sendinblue_cache");
	foreach ($tables as $tbl) {
		$row = $wpdb->get_row("SHOW TABLES LIKE '" . $table . "'", ARRAY_A);
		if (safe_count($row) > 0) {
			$sql = "DROP TABLE $table";
			$wpdb->query($table);
		}
	}
}

register_activation_hook(__FILE__, "sendinblue_plugin_activation");
register_deactivation_hook(__FILE__, "sendinblue_plugin_deactivation");
